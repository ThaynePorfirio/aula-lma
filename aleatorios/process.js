const {createApp} = Vue;
createApp({
    data(){
        return {
            randomIndex: 0,
            randomIndexInternet: 0,

            //vetor de imagens locais
            imagensLocais:[
                './Imagens/lua.jpg',
                './Imagens/SENAI_logo.png',
                './Imagens/sol.jpg'
            ],

            imagensInternet:[
                'http://pm1.narvii.com/6788/9c22c333ff0b10d153a8b4e15268cc596fd255e9v2_00.jpg',
                'https://pbs.twimg.com/media/FCCNRR1XEAQWsvB.jpg',
                'https://recreio.uol.com.br/media/_versions/legacy/2021/01/15/bob-esponja-1226720_widelg.jpg'
            ],

        };//Fim return
    },//Fim data

    computed:{
        randomImage()
            {
                return this.imagensLocais[this.randomIndex];
            },//Fim randomImage

            randomImageInternet()
            {
                return this.imagensInternet[this.randomIndexInternet];
            }//Fim randomInternet
    },//Fim computed

    methods:{
        getRandomImage()
        {
            this.randomIndex=Math.floor(Math.random()*this.imagensLocais.length);
            this.randomIndexInternet=Math.floor(Math.random()*this.imagensInternet.length);
        }
    },//Fim methods

}).mount("#app");